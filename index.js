const DEFAULT_ENDPOINT = 'https://rest.quicklizard.com',
      unirest = require('unirest'),
      querystring = require('querystring'),
      crypto = require('crypto');

/**
 * Unix UTC timestamp getter
 * @returns {number} Unix UTC timestamp in seconds
 */
function getUnixTimestampUTC() {
  return new Date().getTime() / 1000;
}

/**
 * API Client constructor function
 * @param endpoint {string} API endpoint (defaults to https://rest.quicklizard.com)
 * @param key {string} API key
 * @param secret {string} API secret
 * @constructor
 */
let Client = function ({ endpoint, key, secret }) {
  this.endpoint = endpoint || DEFAULT_ENDPOINT;
  this.apiKey = key;
  this.apiSecret = secret;
  if (typeof key === 'undefined' || typeof secret === 'undefined') {
    throw new Error('API key / secret are required');
  }
};

/**
 * Issue HTTP GET API requests
 * @param path {string} request path
 * @param params {object} request parameters (converted into a query-string)
 * @param cb {function} request end callback
 */
Client.prototype.get = function ({ path, params, cb }) {
  params = params || {};
  params['qts'] = getUnixTimestampUTC();
  let queryString = querystring.stringify(params);
  let digest = this.signRequest({ path, queryString });
  let url = this.apiURL({ path, queryString });
  unirest.get(url).headers({'Accept': 'application/json', 'Content-Type': 'application/json', 'API_KEY': this.apiKey, 'API_DIGEST': digest}).end(cb);
};

/**
 * Issue HTTP POST API requests
 * @param path {string} request path
 * @param params {object} request parameters (converted into a query-string)
 * @param body {object} request body
 * @param cb {function} request end callback
 */
Client.prototype.post = function ({ path, params, body, cb }) {
  params = params || {};
  params['qts'] = getUnixTimestampUTC();
  let queryString = querystring.stringify(params);
  let digest = this.signRequest({ path, queryString, body });
  let url = this.apiURL({ path, queryString });
  unirest.post(url).headers({'Accept': 'application/json', 'Content-Type': 'application/json', 'API_KEY': this.apiKey, 'API_DIGEST': digest}).send(body).end(cb);
};

/**
 * Issue HTTP PUT API requests
 * @param path {string} request path
 * @param params {object} request parameters (converted into a query-string)
 * @param body {object} request body
 * @param cb {function} request end callback
 */
Client.prototype.put = function ({ path, params, body, cb }) {
  params = params || {};
  params['qts'] = getUnixTimestampUTC();
  let queryString = querystring.stringify(params);
  let digest = this.signRequest({ path, queryString, body });
  let url = this.apiURL({ path, queryString });
  unirest.put(url).headers({'Accept': 'application/json', 'Content-Type': 'application/json', 'API_KEY': this.apiKey, 'API_DIGEST': digest}).send(body).end(cb);
};

/**
 * Issue HTTP POST API requests with file upload
 * @param path {string} request path
 * @param params {object} request parameters (converted into a query-string)
 * @param uploadPath {string} file upload local path
 * @param cb {function} request end callback
 */
Client.prototype.upload = function ({ path, params, uploadPath, cb }) {
  params = params || {};
  params['qts'] = getUnixTimestampUTC();
  let queryString = querystring.stringify(params);
  let digest = this.signRequest({ path, queryString });
  let url = this.apiURL({ path, queryString });
  unirest.post(url).headers({'Accept': 'application/json', 'Content-Type': 'multipart/form-data', 'API_KEY': this.apiKey, 'API_DIGEST': digest}).attach('upload', uploadPath).end(cb);
};

/**
 * Creates API request signature based on request parts and API secret
 * @param path {string} request path
 * @param queryString {string} request query-string
 * @param body {object} request body (POST/PUT)
 * @returns {string} API signature
 */
Client.prototype.signRequest = function ({ path, queryString, body }) {
  let ts = getUnixTimestampUTC();
  let bodyString = typeof body === 'undefined' ? '' : JSON.stringify(body);
  let parts = [path, queryString, bodyString, this.apiSecret].join('');
  return crypto.createHash('sha256').update(parts).digest('hex');
};

/**
 * API request URL builder
 * @param path {string} request path
 * @param queryString {string} request query-string
 * @returns {string} API request full URL
 */
Client.prototype.apiURL = function ({ path, queryString }) {
  return `${this.endpoint}${path}?${queryString}`;
}

module.exports = { Client };