# Quicklizard API Client - Node

Authenticate and perform operations against Quicklizard's REST API from your Ruby application.

## Installation

Install from Bitbucket - `npm install --save git+ssh://git@bitbucket.org/quicklizard/ql-api-client-node.git`

## Usage

```javascript
const Client = require('ql-api-client-node').Client,
      apiKey = 'test',    // replace with your actual API key
      apiSecret = 'test'; // replace with your actual API secret
let client = new Client({ key: apiKey, secret: apiSecret });

// create a client with a different API endpoint for testing purposes
let stagingClient = new Client({ key: apiKey, secret: apiSecret, endpoint: 'https://rest-staging.quicklizard.com' });
```

### GET Requests

```javascript
const Client = require('ql-api-client-node').Client,
      apiKey = 'test',    // replace with your actual API key
      apiSecret = 'test', // replace with your actual API secret
      clientKey = 'test_client';
let client = new Client({ key: apiKey, secret: apiSecret });
client.get({ path: '/api/v2/echo', params: {'client_key': clientKey}}, function(response) {
  console.log(response);
});
```
---
### POST Requests

```javascript
const Client = require('ql-api-client-node').Client,
      apiKey = 'test',    // replace with your actual API key
      apiSecret = 'test', // replace with your actual API secret
      clientKey = 'test_client';
let client = new Client({ key: apiKey, secret: apiSecret });
client.post({ path: '/api/v2/echo', body: {'client_key': clientKey}, cb: function(response) {
  console.log(response);
}});
```
---
### PUT Requests

```javascript
const Client = require('ql-api-client-node').Client,
      apiKey = 'test',    // replace with your actual API key
      apiSecret = 'test', // replace with your actual API secret
      clientKey = 'test_client';
let client = new Client({ key: apiKey, secret: apiSecret });
client.put({ path: '/api/v2/echo', body: {'client_key': clientKey}, cb: function(response) {
  console.log(response);
}});
```
---
### File Upload Requests

```javascript
const Client = require('ql-api-client-node').Client,
      apiKey = 'test',    // replace with your actual API key
      apiSecret = 'test', // replace with your actual API secret
      clientKey = 'test_client',
      uploadPath = '/tmp/my_file.txt';
let client = new Client({ key: apiKey, secret: apiSecret });
client.upload({ path: '/api/v2/echo', params: {'client_key': clientKey}, uploadPath: uploadPath, cb: function(response) {
  console.log(response);
}});
```
---

## Method Signature

Look inside `test/test_client.js` for a complete example on how to use the API client.

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/quicklizard/ql-api-client-node.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

