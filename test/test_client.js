const expect = require('chai').expect,
      assert = require('chai').assert,
      _path = require('path'),
      Client = require('../index').Client,
      apiKey = 'test',
      apiSecret = 'test';

describe('Client', () => {
  describe('#constructor', () => {
    it('should construct a new API client instance', () => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      assert.equal(client.endpoint, endpoint);
    })
  });

  describe('#signRequest()', () => {
    it('should create a correct API signature', () => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/dummy';
      let queryString = 'test=1';
      let digest = client.signRequest({ path, queryString });
      assert.equal(digest, 'be2b1d6e6e194438ad151d730861e3be735769d56e955ecf8dfc833aef2ed559');
    })
  });

  describe('#apiURL()', () => {
    it('should create a correct API request URL', () => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/dummy';
      let queryString = 'test=1';
      let url = client.apiURL({ path, queryString });
      assert.equal(url, 'https://rest-staging.quicklizard.com/api/v2/dummy?test=1');
    })
  });

  describe('#get()', () => {
    it('should return status 200', (done) => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/echo';
      let params = {'test': 1, client_key: 'test_client'};
      let cb = function (response) {
        assert.equal(response.statusCode, 200);
        done()
      }
      client.get({ path, params, cb });
    });
    it('should return account details in response body', (done) => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/echo';
      let params = {'test': 1, client_key: 'test_client'};
      let cb = function (response) {
        let body = JSON.parse(response.raw_body);
        assert.equal(body.account.name, 'Test Account');
        done()
      }
      client.get({ path, params, cb });
    });
  });

  describe('#post()', () => {
    it('should return status 200', (done) => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/echo';
      let body = {'test': 1, client_key: 'test_client'};
      let cb = function (response) {
        assert.equal(response.statusCode, 200);
        done()
      }
      client.post({ path, body, cb });
    });
    it('should return account details in response body', (done) => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/echo';
      let body = {'test': 1, client_key: 'test_client'};
      let cb = function (response) {
        assert.equal(response.raw_body.account.name, 'Test Account');
        done()
      }
      client.post({ path, body, cb });
    });
  });

  describe('#put()', () => {
    it('should return status 200', (done) => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/echo';
      let body = {'test': 1, client_key: 'test_client'};
      let cb = function (response) {
        assert.equal(response.statusCode, 200);
        done()
      }
      client.put({ path, body, cb });
    });
    it('should return account details in response body', (done) => {
      let endpoint = 'https://rest-staging.quicklizard.com';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/echo';
      let body = {'test': 1, client_key: 'test_client'};
      let cb = function (response) {
        assert.equal(response.raw_body.account.name, 'Test Account');
        done()
      }
      client.put({ path, body, cb });
    });
  });
  
  describe('#upload()', () => {
    it('should return status 200', (done) => {
      let endpoint = 'http://localhost:3000';
      let client = new Client({ endpoint, key: apiKey, secret: apiSecret });
      let path = '/api/v2/products/upload';
      let params = {client_key: 'test_client'};
      let uploadPath = _path.resolve('assets/upload_template.txt');
      let cb = function (response) {
        let body = JSON.parse(response.raw_body);
        assert.equal(response.statusCode, 200);
        assert.equal(body.status, 'OK');
        done()
      }
      client.upload({ path, params, uploadPath, cb });
    });
  });
  
});